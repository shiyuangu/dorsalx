#!/usr/bin/env bash
# dorsalx.sh --- 
# Copyright (C)2013  S. Gu
# author: S. Gu <sgu@anl.gov>
# brief: A building script works with GNU Modules and GNU Global
# with the purpose to create multiple build of a package and
# allow easy switch between different build. This is modified
# from the building script Dorsal of FEniCS.

# Please run `export -f module` first 


#Mark variables which are modified and created for export. 
set -a


# Colours for progress and error reporting
BAD="\033[1;31m"
GOOD="\033[1;32m"
BOLD="\033[1m"

prettify_dir() {
   # Make a directory name more readable by replacing homedir with "~"
   echo ${1/#$HOME\//~\/}
}

cecho() {
    # Display messages in a specified colour
    COL=$1; shift
    echo -e "${COL}$@\033[0m"
}

default () {
    # Export a variable, if it is not already set
    VAR="${1%%=*}"
    VALUE="${1#*=}"
    eval "[[ \$$VAR ]] || export $VAR='$VALUE'"
}

quit_if_fail() {
    # Exit with some useful information if something goes wrong
    STATUS=$?
    if [ ${STATUS} -ne 0 ]
    then
	cecho ${BAD} 'Failure with exit status:' ${STATUS}
	cecho ${BAD} 'Exit message:' $1
	cecho ${BAD} '\$PWD=' $PWD
	exit ${STATUS}
    fi
}

# generate a GNU module file
generate_modulefile(){
    cecho ${GOOD} "Generating module file"
     #convert the LNAME from lowercase to uppercase
    LNAME_SRC_DIR_VAR=`echo ${LNAME}|tr '[:lower:]' '[:upper:]' `_SRC_DIR
    default $LNAME_SRC_DIR_VAR=${DOWNLOAD_PATH}/${LNAME}/${FNAME}
    eval "LNAME_SRC_DIR_VAL=\$$LNAME_SRC_DIR_VAR"
    
    LNAME_DIR_VAR=`echo ${LNAME}|tr '[:lower:]' '[:upper:]' `_DIR
    default $LNAME_DIR_VAR=${INSTALL_PATH}
    eval "LNAME_DIR_VAL=\$$LNAME_DIR_VAR"

    default PATH_VAL=$LNAME_DIR_VAL/bin
    default LD_LIBRARY_PATH_VAL=$LNAME_DIR_VAL/lib64:$LNAME_DIR_VAL/lib
    default DYLD_LIBRARY_PATH_VAL=$LNAME_DIR_VAL/lib64:$LNAME_DIR_VAL/lib
    
    echo "#%Module1.0#"> modulefile
    echo "# ${LNAME}/${FNAME} module files" >> modulefile
    echo "conflict ${LNAME}" >> modulefile 
    echo "setenv ${LNAME_DIR_VAR} ${LNAME_DIR_VAL}" >> modulefile
    echo "setenv ${LNAME_SRC_DIR_VAR} ${DOWNLOAD_PATH}/${LNAME}/${FNAME}" >> modulefile
    echo "prepend-path PATH ${PATH_VAL}" >> modulefile
    echo "prepend-path LD_LIBRARY_PATH ${LD_LIBRARY_PATH_VAL}" >>modulefile
    echo "prepend-path DYLD_LIBRARY_PATH ${DYLD_LIBRARY_PATH_VAL}" >>modulefile
    echo "prepend-path GTAGSLIBPATH ${LNAME_SRC_DIR_VAL}" >> modulefile

    #unset temporary variable to eliminate the influence between packages
    #on the same cfgx file. 
    unset $LNAME_SRC_DIR_VAR
    unset $LNAME_DIR_VAR
    cecho ${GOOD} "modulefile is generated"
}

package_fetch () {
    # First, make sure we're in the right directory before downloading
    cd ${DOWNLOAD_PATH}
    mkdir -p ${LNAME}
    cd ${LNAME}
    
    cecho ${GOOD} "Fetching ${DIST_NAME}"

    # Fetch the package appropriately from its source
    if [ ${PACKING} = ".tar.bz2" ] || [ ${PACKING} = ".tar.gz" ] || [ ${PACKING} = ".tbz2" ] || [ ${PACKING} = ".tgz" ] || [ ${PACKING} = ".tar.xz" ] 
    then
      # Only download tarballs that do not exist
      if [ ! -e ${DIST_NAME}${PACKING} ]
      then
	if [[ `echo $SOURCE|cut -b1-8` == "file:///" ]]
	then 
	   cp `echo ${SOURCE}${DIST_NAME}${PACKING}|cut -b8-` .
	   quit_if_fail "Fail to copy local files"
	else 
           if [ ${STABLE_BUILD} = false ] && [ ${USE_SNAPSHOTS} = true ]
           then
             wget --retry-connrefused --no-check-certificate --server-response -c ${SOURCE}${DIST_NAME}${PACKING}
           else
             wget --retry-connrefused --no-check-certificate -c ${SOURCE}${DIST_NAME}${PACKING}
           fi
	fi #if [[`echo $SOURCE...` ]]   
      fi  #[ ! -e ${DIST_NAME}${PACKING} ]

      # Download again when using snapshots and unstable packages, but
      # only when the timestamp has changed
      if [ ${STABLE_BUILD} = false ] && [ ${USE_SNAPSHOTS} = true ]
      then
	if [[ `echo $SOURCE|cut -b1-8` == "file:///" ]]
	then
	   cp `echo ${SOURCE}${DIST_NAME}${PACKING}|cut -b8-` .
	    quit_if_fail "Fail to copy local files"
	else    
           wget --timestamping --retry-connrefused --no-check-certificate ${SOURCE}${DIST_NAME}${PACKING}
	fi  #[[ `echo $SOURCE|cut -b1-8` == "file:///" ]]  
      fi
    elif [ ${PACKING} = "hg" ]
    then
      # Suitably clone or update hg repositories
      if [ ! -d ${FNAME} ]
      then
        hg clone ${SOURCE}${DIST_NAME} ${FNAME}
      else
        cd ${FNAME}
        hg pull --update
        cd ..
      fi
    elif [ ${PACKING} = "svn" ]
    then
      # Suitably check out or update svn repositories
      if [ ! -d ${FNAME} ]
      then
  	    svn co ${SOURCE} ${FNAME}
      else
        cd ${FNAME}
        svn up
        cd ..
      fi
    elif [ ${PACKING} = "git" ]
    then
      # Suitably clone or update git repositories
      if [ ! -d ${FNAME} ]
      then
        git clone ${SOURCE}${DIST_NAME} ${FNAME}
	cd ${FNAME}
	git branch --set-upstream master origin/master
      else
        cd ${FNAME}
        git pull
        cd ..
      fi
    elif [ ${PACKING} = "gitsvn"  ]
    then
	if [ ! -d ${FNAME} ]
	then
	    git svn clone ${SOURCE} ${FNAME}
	else
	    cd ${FNAME}
	    git svn rebase
	    cd ..
	fi    
    elif [ ${PACKING} = "bzr" ]
    then
      # Suitably branch or update bzr repositories
      #FIXME: check the syntax for renaming bzr branch 
      if [ ! -d ${FNAME} ]
      then
        bzr branch ${SOURCE}${DIST_NAME}
      else
        cd ${DIST_NAME}
        bzr pull
        cd ..
      fi
    else 
       cecho ${BAD} "Don't know how to fetch PACKING type ${PACKING}"
       exit 1 
    fi

    # Quit with a useful message if something goes wrong
    quit_if_fail "Error fetching ${DIST_NAME}."
}

package_unpack() {
    # First make sure we're in the right directory before unpacking
    cd ${DOWNLOAD_PATH}/${LNAME}

    # Only need to unpack tarballs
    if [ ${PACKING} = ".tar.bz2" ] || [ ${PACKING} = ".tar.gz" ] ||  [ ${PACKING} = ".tbz2" ] || [ ${PACKING} = ".tgz" ] || [ ${PACKING} = ".tar.xz" ]
    then
      cecho ${GOOD} "Unpacking ${LNAME}/${FNAME}"
      # Make sure the tarball was downloaded
      if [ ! -e ${DIST_NAME}${PACKING} ]
      then
        cecho ${BAD} "${DIST_NAME}${PACKING} does not exist. Please download first."
	cecho ${BAD} "\$PWD=$PWD"
        exit 1
      fi

      # Unpack the archive only if it isn't already or when using
      # snapshots and unstable packages
      #FIXME: The following doesn't handle corrupted unpacking correctly.
      if [ ${STABLE_BUILD} = false ] && [ ${USE_SNAPSHOTS} = true ] || [ ! -d "${DIST_NAME}" ]
      then
        # Unpack the archive in accordance with its packing
        if [ ${PACKING} = ".tar.bz2" ] || [ ${PACKING} = ".tbz2" ]
        then
          tar xjf ${DIST_NAME}${PACKING}
        elif [ ${PACKING} = ".tar.gz" ] || [ ${PACKING} = ".tgz" ]
        then
          tar xzf ${DIST_NAME}${PACKING}
        elif [ ${PACKING} = ".tar.xz" ]
        then
	  cecho ${BAD} "PWD=${PWD}, DIST_NAME=${DIST_NAME}" 
          tar xJf ${DIST_NAME}${PACKING}
        elif [ ${PACKING} = ".zip" ]
        then
          unzip ${DIST_NAME}${PACKING}
	fi
      fi
      package_specific_postunpack
      if [ ! -d ${FNAME} ]
      then
	  mv ${DIST_NAME} ${FNAME}
      fi
    fi

    # Quit with a useful message if something goes wrong
    quit_if_fail "Error unpacking ${DIST_NAME}."
    cecho ${GOOD} "Unpacking has completed."
}

package_build() {
    # Get things ready for the compilation process
    cecho ${GOOD} "Building ${LNAME}/${FNAME}"
    
    #Make sure the source has been unpacked/checkout 
    cd ${DOWNLOAD_PATH}/${LNAME}
    if [ ! -d "${FNAME}" ]
    then
        cecho ${BAD} "${DOWNLOAD_PATH}/${LNAME}/${FNAME} does not exist -- please unpack first."
        exit 1
    fi
    SRCDIR=.

    # Create build directory if it does not exist
    if [ ! -d ${BUILD_DIR} ]
    then
        mkdir -p ${BUILD_DIR}
    fi

    # Move to the build directory
    cd ${BUILD_DIR}

    # Carry out any package-specific setup
    package_specific_setup
    quit_if_fail "There was a problem in build setup for ${PACKAGE}."

    # Use the appropriate build system to compile and install the
    # package
    for cmd_file in dorsalx_configure dorsalx_build; do
	echo "#!/usr/bin/env bash" >${cmd_file}
	chmod a+x ${cmd_file}

        # Write variables to files so that they can be run stand-alone
	declare -x >>${cmd_file}

        # From this point in dorsalx_*, errors are fatal
	echo "set -e" >>${cmd_file}
    done

    if [ ${BUILDCHAIN} = "autotools" ]
    then
        if [ -f ${SRCDIR}/configure ]
        then
	    echo ${SRCDIR}/configure ${CONFOPTS} --prefix=${INSTALL_PATH} >>dorsalx_configure
        fi
        for target in "${TARGETS[@]}"
        do
            echo make ${MAKEOPTS} -j ${PROCS} $target >>dorsalx_build
        done
    elif [ ${BUILDCHAIN} = "autotools-no-prefix" ]
    then
	if [ -f ${SRCDIR}/configure ]
        then
	    echo ${SRCDIR}/configure ${CONFOPTS} >>dorsalx_configure
        fi
           echo make ${MAKEOPTS} -j ${PROCS} >>dorsalx_build
    elif [ ${BUILDCHAIN} = "python" ]
    then
        echo python setup.py install --prefix=${INSTALL_PATH} >>dorsalx_build
    elif [ ${BUILDCHAIN} = "scons" ]
    then
        for target in "${TARGETS[@]}"
        do
            echo scons -j ${PROCS} ${SCONSOPTS} prefix=${INSTALL_PATH} $target >>dorsalx_build
        done
    elif [ ${BUILDCHAIN} = "cmake" ]
    then
        BUILD_DIR="./dorsalx_build_dir"
        echo mkdir -p ${BUILD_DIR} >>dorsalx_configure
        echo cd ${BUILD_DIR} >>dorsalx_configure
        echo cmake ${CONFOPTS} -DCMAKE_INSTALL_PREFIX=${INSTALL_PATH} ../ >>dorsalx_configure
        for target in "${TARGETS[@]}"
        do
            echo make -C ${BUILD_DIR} ${MAKEOPTS} -j ${PROCS} $target >>dorsalx_build
        done
    elif [ ${BUILDCHAIN} = "custom" ]
    then
        # Write the function definition to file
        declare -f package_specific_build >>dorsalx_build
        echo package_specific_build >>dorsalx_build
    fi
    echo "touch dorsalx_successful_build" >> dorsalx_build

    # Run the generated build scripts
    if [ ${BASH_VERSINFO} -ge 3 ]
    then
	set -o pipefail
	./dorsalx_configure 2>&1 | tee dorsalx_configure.log
    else
	./dorsalx_configure
    fi
    quit_if_fail "There was a problem configuring ${PACKAGE}."

    if [ ${BASH_VERSINFO} -ge 3 ]
    then
	set -o pipefail
	./dorsalx_build 2>&1 | tee dorsalx_build.log
    else
	./dorsalx_build
    fi
    quit_if_fail "There was a problem building ${PACKAGE}."

    # Carry out any package-specific post-build instructions
    package_specific_install
    if [ ! ${BUILDED_DIR} == ${BUILD_DIR} ]
    then
	mv dorsalx_configure ${BUILDED_DIR}/dorsalx_configure
	mv dorsalx_build ${BUILDED_DIR}/dorsalx_build
	mv dorsalx_configure.log ${BUILDED_DIR}/dorsalx_configure.log
	mv dorsalx_build.log ${BUILDED_DIR}/dorsalx_build.log
	mv dorsalx_successful_build ${BUILDED_DIR}/dorsalx_successful_build
    fi 
    quit_if_fail "There was a problem in post-build instructions for ${PACKAGE}."
    cecho ${GOOD} "Build has completed"
}

package_register() {
    # Get ready to set environment variables related to the package
    if [ ! -d "${BUILDED_DIR}" ]
    then
        cecho ${BAD} "${BUILDED_DIR} does not exist -- please install at least once."
        exit 1
    fi

    # Move to the package directory
    cd ${BUILDED_DIR}

    
    
    #generate GNU modulefile
    generate_modulefile
    # Set any package-specific environment variables
    package_specific_register
    #copy modulefile to ${MODULE_PATH}
    mkdir -p ${MODULE_PATH}/${LNAME}
    if [ ! "${MODULEFILE_NAME}" ]
    then
	if [ ! "${ID}" ]
	then
	    MODULEFILE_NAME=${FNAME}
	else
	    MODULEFILE_NAME=${FNAME}-${ID}
	fi
    fi
    cp modulefile ${MODULE_PATH}/${LNAME}/${MODULEFILE_NAME}
    
    quit_if_fail "There was a problem registering ${PACKAGE}."
    cecho ${GOOD} "Register has completed"
}

guess_platform() {
  # Try to guess the name of the platform we're running on
  if [ -f /usr/bin/cygwin1.dll ]
  then
      echo xp
  elif [ -f /etc/fedora-release ]
  then
      local FEDORANAME=`gawk '{if (match($0,/\((.*)\)/,f)) print f[1]}' /etc/fedora-release`
      case ${FEDORANAME} in
          Cambridge*)   echo fedora10;;
          Leonidas*)    echo fedora11;;
          Constantine*) echo fedora12;;
          Goddard*)     echo fedora13;;
          Laughlin*)    echo fedora14;;
          Lovelock*)    echo fedora15;;
      esac
  elif [ -x /usr/bin/sw_vers ]
  then
    local MACOSVER=$(sw_vers -productVersion)
    case ${MACOSVER} in
      10.4*) 	echo tiger;;
      10.5*)	echo leopard;;
      10.6*)	echo snowleopard;;
      10.7*)    echo lion;;
      10.8*)    echo mountainlion;;
    esac
  elif [ -x /usr/bin/lsb_release ]; then
    local DISTRO=$(lsb_release -i -s)
    local CODENAME=$(lsb_release -c -s)
    local DESCRIPTION=$(lsb_release -d -s)
    case ${DISTRO}:${CODENAME}:${DESCRIPTION} in
      Ubuntu:*:*)            echo ${CODENAME};;
      Debian:*:*)            echo ${CODENAME};;
      Gentoo:*:*)            echo gentoo;;
      *:Nahant*:*)           echo rhel4;;
      *:Tikanga*:*)          echo rhel5;;
      Scientific:Carbon*:*)  echo rhel6;;
      *:*:*CentOS*\ 4*)      echo rhel4;;
      *:*:*CentOS*\ 5*)      echo rhel5;;
      *:*:*openSUSE\ 11.1*)  echo opensuse11.1;;
      *:*:*openSUSE\ 11.2*)  echo opensuse11.2;;
      *:*:*openSUSE\ 11.3*)  echo opensuse11.3;;
      *:*:*openSUSE\ 11.4*)  echo opensuse11.4;;
      *:*:*openSUSE\ 12.1*)  echo opensuse12.1;;
    esac
  fi
}

guess_architecture() {
  # Try to guess the architecture of the platform we are running on
    ARCH=unknown
    if [ -x /usr/bin/uname -o -x /bin/uname ]
    then
	ARCH=`uname -m`
    fi
}



### Start the build process ###

export ORIG_DIR=`pwd`

# Read configuration variables from the $1
if [ $# -eq 0 ]
then
    cecho ${BAD} "Syntax: dorsalx file.cfgx"
    cecho ${BAD} "A cfgx file is a shell script which defines the follow variables(Cf. Readme.txt for details about the formats of cfgx files and package files)"
    exit 1
else
    #unset all mandatory variables in case unintentionally set 
    unset DOWNLOAD_PATH
    unset INSTALL_PATH
    unset PACKAGES_PATH
    unset MODULE_PATH
    if [ -f $1 ]
    then 
	source $1
    else 
	cecho ${BAD} "file $1 doesn't exist"
    fi
fi
# For changes specific to your local setup or for debugging, use local.cfg
#if [ -f local.cfg ]
#then
#    source local.cfg
#fi
if [[ -z "${MODULE_PATH}" ]] || [[ ! -d "${MODULE_PATH}" ]]
then
    cecho ${BAD} "\${MODULE_PATH}(=${MODULE_PATH}) is not specified or doesn't exit"
    exit 1
fi 
if [[ -z "${DOWNLOAD_PATH}" ]]
then
   cecho ${BAD} "Please specify variable DOWNLOAD_PATH: The directory contains the source."
   exit 1
fi
if [[ -z "${INSTALL_PATH}" ]]
then
   cecho ${BAD} "Please specify variable INSTALL_PATH: The directory contains the source."
   exit 1
fi

if [[ -z "${PACKAGES_PATH}" ]]
then
   cecho ${BAD} "Please specify variable PACKAGES_PATH: The directory contains the source."
   exit 1
fi
# If any variables are missing, revert them to defaults
default PROCS=1
default STABLE_BUILD=false
default USE_SNAPSHOTS=false
# Check if dorsalx.sh was invoked correctly
if [ $# -eq 1 ]
then
    echo "-------------------------------------------------------------------------------"
    # Show the initial comments in the platform file, as it often
    # contains instructions about packages that should be installed
    # first, etc. Remove first field '#' so that cut-and-paste of
    # e.g. apt-get commands is easy.
    #awk '/^##/ {exit} {$2=""; print}' <${PLATFORM}
    echo
    echo "Downloading files to:   $(prettify_dir ${DOWNLOAD_PATH})"
    echo "Installing projects in: $(prettify_dir ${INSTALL_PATH})"
    echo
    echo "------------------------------------------------------------------------------"
fi


# If the platform doesn't override the system python by installing its
# own, figure out the version of of the existing python
default PYTHONVER=`python -c "import sys; print sys.version[:3]"`

# Create necessary directories and set appropriate variables
mkdir -p ${DOWNLOAD_PATH}
mkdir -p ${INSTALL_PATH}/bin
mkdir -p ${INSTALL_PATH}/conf
mkdir -p ${INSTALL_PATH}/include
mkdir -p ${INSTALL_PATH}/lib
mkdir -p ${INSTALL_PATH}/lib/python${PYTHONVER}/site-packages
mkdir -p ${INSTALL_PATH}/share
export PATH=${INSTALL_PATH}/bin:${PATH}
export LD_LIBRARY_PATH=${INSTALL_PATH}/lib:${LD_LIBRARY_PATH}
export DYLD_LIBRARY_PATH=${INSTALL_PATH}/lib:${DYLD_LIBRARY_PATH}
export PYTHONPATH=${INSTALL_PATH}/lib/python${PYTHONVER}/site-packages:${PYTHONPATH}
export PKG_CONFIG_PATH=${INSTALL_PATH}/lib/pkgconfig:${PKG_CONFIG_PATH}
ORIG_PROCS=${PROCS}
# Add some extra library paths for 64 bit machines
guess_architecture
if [ "$ARCH" == "x86_64" ]; then
    export LD_LIBRARY_PATH=${INSTALL_PATH}/lib64:${LD_LIBRARY_PATH}
    export DYLD_LIBRARY_PATH=${INSTALL_PATH}/lib64:${DYLD_LIBRARY_PATH}
    export PYTHONPATH=${INSTALL_PATH}/lib64/python${PYTHONVER}/site-packages:${PYTHONPATH}
fi

# Fetch and build individual packages
for PACKAGE in ${PACKAGES[@]}
do
    # Return to the main Dorsal directory
    cd ${ORIG_DIR}

    # Skip building this package if the user requests it
    SKIP=false
    case ${PACKAGE} in
	skip:*) SKIP=true;  PACKAGE=${PACKAGE#*:};;
	once:*) SKIP=maybe; PACKAGE=${PACKAGE#*:};;
    esac

    # Check if the package exists
    if [ ! -e ${PACKAGES_PATH}/${PACKAGE}.package ]
    then
        cecho ${BAD} "${PACKAGES_PATH}/${PACKAGE}.package does not exist yet. Please create it."
        exit 1
    fi
    cecho ${GOOD} "Begin package ${PACKAGE}"
    # Reset package-specific variables
    unset LNAME
    unset FNAME
    unset ID
    unset DIST_NAME
    unset SOURCE
    unset PACKING
    unset BUILDCHAIN
    unset CONFOPTS
    unset MAKEOPTS
    unset TARGETS
    unset SCONSOPTS
    unset BUILD_DIR
    unset BUILDED_DIR
    unset MODULEFILE_NAME
    unset PATH_VAL
    unset LD_LIBRARY_PATH_VAL
    unset DYLD_LIBRARY_PATH_VAL
    
    TARGETS=('' install)
    PROCS=${ORIG_PROCS}

    # Reset package-specific functions
    package_specific_postunpack() { true; }
    package_specific_setup () { true; }
    package_specific_build () { true; }
    package_specific_install () { true; }
    package_specific_register () { true; }

    # Fetch information pertinent to the package
    source ${PACKAGES_PATH}/${PACKAGE}.package
    # Turn to a stable version of the package if that's what the user
    # wants and it exists
    if [ ${STABLE_BUILD} = true ] && [ -e ${PACKAGES_PATH}/${PACKAGE}-stable.package ]
    then
      source ${PACKAGES_PATH}/${PACKAGE}-stable.package
    elif [ ${STABLE_BUILD} = false ] && [ ${USE_SNAPSHOTS} = true ] && [ -e ${PACKAGES_PATH}/${PACKAGE}-snapshot.package ]
    then
      source ${PACKAGES_PATH}/${PACKAGE}-snapshot.package
    fi

    # Ensure that the package file is sanely constructed
    if [ ! "${DIST_NAME}" ] || [ ! "${SOURCE}" ] || [ ! "${PACKING}" ] || [ ! "${BUILDCHAIN}" ] || [ ! "${LNAME}" ] || [ ! "${FNAME}" ]
    then
        cecho ${BAD} "${PACKAGE}.package is not properly formed. Please check that all necessary variables (DIST_NAME,SOURCE, PACKING, BUILDCHAIN, LNAME, FNAME) are defined."
        exit 1
    fi

    # Most packages extract to a directory named after the package
    default BUILD_DIR=${DOWNLOAD_PATH}/${LNAME}/${FNAME}
    default BUILDED_DIR=${BUILD_DIR}
    if [ ${SKIP} = maybe ] && [ ! -f ${BUILDED_DIR}/dorsalx_successful_build ]
    then
	SKIP=false
    fi

    # Fetch, unpack and build package
    if [ ${SKIP} = false ]
    then
      # Fetch, unpack and build the current package
      package_fetch
      package_unpack
      package_build
      package_register
    else
      # Let the user know we're skipping the current package
      cecho ${GOOD} "Skipping ${PACKAGE} (BUILDED_DIR=${BUILDED_DIR})"
    fi
done

cecho ${GOOD} "All packages have finished."

